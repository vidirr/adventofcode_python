houses = set()

def get_next_pos(m, x, y):
	
	if m == "v": y -= 1
	if m == "^": y += 1
	if m == "<": x -= 1
	if m == ">": x += 1
	return x, y


with open('input.txt') as f:
	
	x , y = 0, 0

	for move in f.read():
		houses.add((x, y))
		x, y = get_next_pos(move, x, y)

print len(houses)