houses = set()

def get_next_pos(m, x, y):
	
	if m == "v": y -= 1
	if m == "^": y += 1
	if m == "<": x -= 1
	if m == ">": x += 1
	return x, y


with open('input.txt') as f:
	
	x , y = 0, 0
	robo_x, robo_y = 0, 0
	robo_turn = False

	houses.add((0, 0))

	for move in f.read():
		
		if robo_turn:
			robo_x, robo_y = get_next_pos(move, robo_x, robo_y)
			houses.add((robo_x, robo_y))
		else:
			x, y = get_next_pos(move, x, y)
			houses.add((x, y))
		robo_turn = not robo_turn


print len(houses)