pos = 1
floor = 0

with open('input.txt') as f:
	for symbol in f.read():
		if symbol == "(": floor += 1
		if symbol == ")": floor -= 1

		if floor == -1:
			print pos
			break
		pos += 1

print "Floor: ", floor