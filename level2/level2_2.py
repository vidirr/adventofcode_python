
total = 0
ribbon = 0

with open('input.txt') as f:
	for box in f:
		l, w, h = map(int, box.strip().split('x'))
		slack = sorted([l * w, w * h, h * l])[0]
		surface = (2 * l * w) + (2 * w * h) + (2 * h * l)
		total += (surface + slack)

		x, y, _ = sorted([l, w, h])
		wrap = (2 * x) + (2 * y)
		bow = l * h * w

		ribbon += (wrap + bow)

print total, ribbon