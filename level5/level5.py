good_vowels = "aeiou"
bad_strings = ["ab", "cd", "pq","xy"]
nice_strings = 0

with open('input.txt') as f:
	for string in f:
		
		string = string.strip()

		bad_string = False
		for i in range(0, len(string) - 1):
			if string[i:i + 2] in bad_strings:
				bad_string = True

		vowel_count = 0		
		for char in good_vowels:
			for c in string:
				if c == char:
					vowel_count += 1


		double_found = False
		for i in range(0, len(string) - 1):
			if string[i] == string[i + 1]:
				double_found = True


		if not bad_string and double_found and vowel_count > 2:
			nice_strings += 1



print nice_strings