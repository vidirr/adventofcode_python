good_vowels = "aeiou"
bad_strings = ["ab", "cd", "pq","xy"]
nice_strings = 0

with open('input.txt') as f:
	for string in f:
		
		string = string.strip()

		same_with_between = False
		for i in range(len(string) - 2):
			if string[i] == string[i + 2]:
				same_with_between = True

		pair = False
		for i in range(len(string) - 1):
			sub = string[i:i+2]

			for j in range(i + 2, len(string) - 1):
				possible_sub = string[j:j + 2]
				if sub == possible_sub:
					pair = True

		if pair and same_with_between:
			nice_strings += 1

print nice_strings